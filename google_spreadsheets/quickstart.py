from __future__ import print_function
from googleapiclient.discovery import build
from apiclient.http import MediaFileUpload
from httplib2 import Http
from oauth2client import file, client, tools
import datetime

SCOPES = ['https://www.googleapis.com/auth/drive'] #'https://www.googleapis.com/auth/drive.file', 'https://www.googleapis.com/auth/spreadsheets']

'''def createSheet():
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)

    drive_service = build('drive', 'v3', credentials=creds)
    
    file_metadata = {
           'name': 'KPI_Report',
           'mimeType': 'application/vnd.google-apps.spreadsheet',
           'parents': ['1wAxLB8FNID2OO2fwqL7cZoiqEUbdSw_0']
    }
    media = MediaFileUpload('files/report.csv',
                        mimetype='text/csv',
                        resumable=True)
    dataFile = drive_service.files().create(body=file_metadata,media_body=media).execute()
    
    print(dataFile)'''

def preprocessCSV(path):
    """THIS IS DOC"""
    #table table.to_csv('files/report.csv') # I use Pandas but use whatever you'd like
    _file = open(path)
    contents = _file.read()
    array = contents.split('\n')
    master_array = []
    for row in array:
        master_array.append(row.split(','))
    return master_array

def updateSheet(*, spreadsheetId, sheetTitle, values):
    """Add a new sheet with name '<sheetTitle>' the to the spreadsheet with the id '<spreadsheetId>'.
    Push the data '<values>' to the new sheet"""
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    
    sheets_service = build('sheets', 'v4', credentials=creds)
    
    update_spreadsheet_body = {"requests": [{"addSheet":{"properties":{"title":sheetTitle}}}]}

    
    responseAddSheet = sheets_service.spreadsheets().batchUpdate(spreadsheetId=spreadsheetId,body=update_spreadsheet_body).execute()
    print(f"### Add new Sheet with name '{sheetTitle}' to file '{spreadsheetId}' : ", responseAddSheet)
    
    spreadsheet_body = {
        "valueInputOption": "USER_ENTERED",
        "data": [{"range":sheetTitle,"values": values}]
    }
    
    response = sheets_service.spreadsheets().values().batchUpdate(spreadsheetId=spreadsheetId, body=spreadsheet_body).execute()
    
    print(f"### Push data to file '{spreadsheetId}': ", response)

if __name__ == '__main__':
    #createSheet()
    sheetTitle= datetime.datetime.utcnow().strftime("%Y-%m-%d/%H:%M")
    values = preprocessCSV('files/report.csv')
    updateSheet(spreadsheetId= '1QS0uSCiyXugwKlCs_FHa3Poryhn1vhv3vcy26S-AK_k', sheetTitle=sheetTitle, values = values)
